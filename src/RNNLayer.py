import torch


class RNNLayer(torch.nn.Module):

    def __init__(self,
                 device,
                 input_size,
                 hidden_size,
                 num_layers,
                 batch_first,
                 dropout,
                 bidirectional,
                 recurrent='lstm'
                 ):

        super(RNNLayer, self).__init__()

        self.device = device

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.batch_first = batch_first
        self.dropout = dropout
        self.bidirectional = bidirectional

        self.recurrent = recurrent

        if self.bidirectional:
            self.num_directions = 2
        else:
            self.num_directions = 1

        if self.recurrent == 'gru':
            self.RNN = torch.nn.GRU(input_size=self.input_size,
                                    hidden_size=self.hidden_size,
                                    num_layers=self.num_layers,
                                    bias=True,
                                    batch_first=self.batch_first,
                                    dropout=self.dropout,
                                    bidirectional=self.bidirectional
                                    ).to(self.device)

        if self.recurrent == 'lstm':
            self.RNN = torch.nn.LSTM(input_size=self.input_size,
                                     hidden_size=self.hidden_size,
                                     num_layers=self.num_layers,
                                     bias=True,
                                     batch_first=self.batch_first,
                                     dropout=self.dropout,
                                     bidirectional=self.bidirectional
                                     ).to(self.device)

        self.init_weights()

    def init_weights(self):

        for m in self.modules():

            if type(m) in [torch.nn.GRU, torch.nn.LSTM, torch.nn.RNN]:

                for name, param in m.named_parameters():

                    if 'weight_ih' in name:
                        torch.nn.init.xavier_uniform_(param.data)

                    elif 'weight_hh' in name:
                        torch.nn.init.orthogonal_(param.data)

                    elif 'bias' in name:
                        param.data.fill_(0)

    def init_hidden(self, batch_size):

        self.batch_size = batch_size

        h_0 = torch.zeros((self.num_layers * self.num_directions,
                           self.batch_size,
                           self.hidden_size
                           ),
                          requires_grad=True
                          ).to(self.device)

        c_0 = torch.zeros((self.num_layers * self.num_directions,
                           self.batch_size,
                           self.hidden_size
                           ),
                          requires_grad=True
                          ).to(self.device)

        if self.recurrent == 'lstm':
            return h_0, c_0

        if self.recurrent == 'gru':
            return h_0

    def forward(self, input, hidden_state):

        output, self.hidden_state = self.RNN(input,
                                             hidden_state
                                             )

        return output, self.hidden_state
