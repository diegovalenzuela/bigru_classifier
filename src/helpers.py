import torch
from src.dataset import get_residues_names
import numpy as np

def run_train_epoch(model, optimizer, data, batch_size, device, clip_by_norm=False, max_norm=None):

    residues = get_residues_names()
    vocabulary_size = len(residues) + 1  # +1 porque hay 1 símbolo para hacer padding

    loss_history = []

    loss = 0

    for batch in range(len(data) - 1):

        # training fase
        model.train()

        # next_batch()
        aminoacid_sequences, angles, positions, aminoacid_sequences_length, coordinates_lengths, secondary_structures, names = data.next_batch()

        if False:
            print()
            print(' NAME : {} '.format(names))
            print(' angles.size() : {} '.format(angles.size()))
            print(' positions.size() : {} '.format(positions.size()))
            print(' lengths : {} '.format(aminoacid_sequences_length[0]))
            print()

        if False:
            print()
            counter_iter = 0
            for element in angles:
                print(counter_iter, '\t', element * 180.0 / np.pi)
                counter_iter += 1
            print()


        # PERMUTATIONS : [batch_size, timesteps, dim]  -->  [timesteps, batch_size, dim]
        aminoacid_sequences = aminoacid_sequences.permute(dims=[1, 0, 2]).data.to(device)  # Long
        secondary_structures = secondary_structures.permute(dims=[1, 0, 2]).data.to(device)  # Long
        angles = angles.permute(dims=[1, 0, 2]).data.to(device).float()  # Float
        positions = positions.permute(dims=[1, 0, 2]).data.to(device).float()  # Float

        # TODO : CHECK THIS !
        input = angles
        target = aminoacid_sequences
        lengths = aminoacid_sequences_length

        # initial hidden state
        hidden_state = model.init_hidden(batch_size=batch_size)

        # forward
        output, hidden_state = model(input=input,
                                     lengths=lengths,
                                     hidden_state=hidden_state
                                     )

        # loss
        # https://pytorch.org/docs/stable/nn.html#torch.nn.CrossEntropyLoss
        loss = torch.nn.CrossEntropyLoss(weight=None,
                                         ignore_index=0,  # zero padding
                                         reduction='elementwise_mean'
                                         )

        # zero grad
        optimizer.zero_grad()

        # compute loss
        loss = loss(input=output.view(-1, vocabulary_size),  # predictions
                    target=target.contiguous().view(-1)
                    )

        # backward
        loss.backward()

        loss += loss.item()

        # clip gradients
        if clip_by_norm and max_norm is not None:
            torch.nn.utils.clip_grad_norm_(parameters=model.parameters(),
                                           max_norm=max_norm,
                                           norm_type=2
                                           )

        # step
        optimizer.step()

        loss_history.append(loss.item())

    loss = 1.0 * loss / (float(len(data) - 1))
    loss = torch.Tensor([loss])

    return model, optimizer, loss, loss_history


def run_evaluation(device, model, data, batch_size):

    residues = get_residues_names()
    vocabulary_size = len(residues) + 1  # +1 porque hay 1 símbolo para hacer padding

    loss = 0
    loss_history = []

    for batch in range(len(data) - 1):  #  for batch in batches

        with torch.no_grad():
            # evaluation fase
            model.eval()

            # next_batch()
            aminoacid_sequences, angles, positions, aminoacid_sequences_length, coordinates_lengths, secondary_structures, names = data.next_batch()

            # PERMUTATIONS : [batch_size, timesteps, dim]  -->  [timesteps, batch_size, dim]
            aminoacid_sequences = aminoacid_sequences.permute(dims=[1, 0, 2]).data.to(device)  # Long
            secondary_structures = secondary_structures.permute(dims=[1, 0, 2]).data.to(device)  # Long
            angles = angles.permute(dims=[1, 0, 2]).data.to(device).float()  # Float
            positions = positions.permute(dims=[1, 0, 2]).data.to(device).float()  # Float

            # TODO : CHECK THIS !
            input = angles
            target = aminoacid_sequences
            lengths = aminoacid_sequences_length

            # initial hidden state
            hidden_state = model.init_hidden(batch_size=batch_size)

            # forward
            output, hidden_state = model(input=input,
                                         lengths=lengths,
                                         hidden_state=hidden_state
                                         )

            # loss
            # https://pytorch.org/docs/stable/nn.html#torch.nn.CrossEntropyLoss
            loss = torch.nn.CrossEntropyLoss(weight=None,
                                             ignore_index=0,  # zero padding
                                             reduction='elementwise_mean'
                                             )

            # compute loss
            loss = loss(input=output.view(-1, vocabulary_size),  # predictions
                        target=target.contiguous().view(-1)
                        )

            loss += loss.item()

            loss_history.append(loss.item())

    loss = 1.0 * loss / (float(len(data) - 1))
    loss = torch.Tensor([loss])

    return loss, loss_history
