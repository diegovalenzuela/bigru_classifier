

def create_PDB(fasta, xyz_backbone, outfile):

    backbone = {"N": "N",
                "CA": "C",
                "C": "C"
                }

    altloc_ = {"N": " ",
               "CA": " ",
               "C": " "
               }

    record_type = "ATOM  "
    chain_id = "A"
    icode = " "
    charge = "  "
    occupancy_str = " "  # * 2

    ofile = open(outfile, "w")

    atom_idx = 0

    for idx, residue in enumerate(fasta):

        for jdx, atom in enumerate(backbone):
            atom_number = atom_idx
            name = atom
            altloc = altloc_[atom]
            resname = residue

            resseq = idx

            x = xyz_backbone[atom_number, 0]
            y = xyz_backbone[atom_number, 1]
            z = xyz_backbone[atom_number, 2]

            bfactor = 1.0

            segid = 0.00

            element = backbone[atom]

            if len(name) < 4 and name[:1].isalpha() and len(element.strip()) < 2:
                name = " " + name

            args = (record_type,        # %s
                    atom_number + 1,    # %5i
                    name,               # %-4s
                    altloc,             # %c
                    resname,            # %3s
                    chain_id,           # %c
                    resseq + 1,         # %4i
                    icode,              # %c
                    x,                  # %8.3f
                    y,                  # %8.3f
                    z,                  # %8.3f
                    occupancy_str,      # %s
                    bfactor,            # %6.2f
                    segid,              # %4s
                    element,            # %2s
                    charge              # %2s
                    )

            template = "%s%5i %-4s%c%3s %c%4i%c   %8.3f%8.3f%8.3f%s%6.2f      %4s%2s%2s\n"

            ofile.write(template % args)

            atom_idx += 1

    template = "%s%5i %-4s%c%3s %c%4i\n"

    args = ("TER   ",
            atom_number + 1,
            " ",
            altloc,
            resname,
            chain_id,
            resseq
            )

    ofile.write(template % args)
    ofile.write('END\n')

    return
