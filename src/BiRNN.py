import torch

from src.RNNLayer import RNNLayer
from src.FullyConnectedLayer import FullyConnectedLayer
from src.EmbeddingLayer import EmbeddingLayer


class BiRNNModel(torch.nn.Module):

    def __init__(self,
                 device,
                 input_size,
                 output_size,
                 embedding_dim,
                 hidden_size,
                 num_layers,
                 batch_first,
                 dropout,
                 bidirectional=True,
                 recurrent='gru',
                 use_embeddings=False
                 ):

        super(BiRNNModel, self).__init__()

        self.device = device

        self.input_size = input_size
        self.output_size = output_size

        self.embedding_dim = embedding_dim

        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.batch_first = batch_first
        self.dropout = dropout
        self.bidirectional = bidirectional
        self.recurrent = recurrent

        if self.bidirectional:
            self.num_directions = 2
        else:
            self.num_directions = 1

        self.use_embeddings = use_embeddings

        if self.use_embeddings:

            self.embedding = EmbeddingLayer(device=self.device,
                                            embedding_dim=self.embedding_dim
                                            )

            self.rnn = RNNLayer(device=self.device,
                                input_size=self.embedding_dim,
                                hidden_size=self.hidden_size,
                                num_layers=self.num_layers,
                                batch_first=self.batch_first,
                                dropout=self.dropout,
                                bidirectional=self.bidirectional,
                                recurrent=self.recurrent
                                )
        else:

            self.rnn = RNNLayer(device=self.device,
                                input_size=self.input_size,
                                hidden_size=self.hidden_size,
                                num_layers=self.num_layers,
                                batch_first=self.batch_first,
                                dropout=self.dropout,
                                bidirectional=self.bidirectional,
                                recurrent=self.recurrent
                                )

        self.fc_linear = FullyConnectedLayer(device=self.device,
                                             input_size=self.hidden_size * self.num_directions,
                                             output_size=self.output_size,
                                             bias=True,
                                             activation_function='linear'
                                             )

    def init_hidden(self, batch_size):

        self.batch_size = batch_size

        return self.rnn.init_hidden(batch_size=batch_size)

    def forward(self, input, lengths, hidden_state):

        if self.use_embeddings:

            # embedding layer
            emb = self.embedding(input)  # [timesteps, batch_size, input_dim, embedding_dim], (input_dim=1)
            input = emb.squeeze(2)  # [timesteps, batch_size, embedding_dim]

        else:

            input = input.float()  # [timesteps, batch_size, input_dim]

        if True:

            # pack padded
            input = torch.nn.utils.rnn.pack_padded_sequence(input,
                                                            lengths=torch.tensor(lengths).to(self.device),
                                                            batch_first=self.batch_first
                                                            ).to(self.device)

            # rnn layer
            output, hidden_state = self.rnn(input=input,
                                            hidden_state=hidden_state
                                            )

            # pad packed
            output = torch.nn.utils.rnn.pad_packed_sequence(output,
                                                            batch_first=self.batch_first,
                                                            padding_value=0.0,
                                                            total_length=None
                                                            )[0]  # [timesteps, batch_size, hidden_size*num_directions]
            if True:
                output = output.view(-1, self.hidden_size * self.num_directions)  # [timesteps*batch_size, hidden_size*num_directions]

            output = self.fc_linear(input=output)  # [timesteps*batch_size, output_size]

            if True:
                output = output.view(-1, self.batch_size, self.output_size)  # [timesteps, batch_size, output_size]

        if False:

            # rnn layer
            output, hidden_state = self.rnn(input=input,
                                            hidden_state=hidden_state
                                            )

            output = output.view(-1, self.hidden_size * self.num_directions)  # [timesteps*batch_size, hidden_size*num_directions]

            output = self.fc_linear(input=output)  # [timesteps*batch_size, output_size]

            output = output.view(-1, self.batch_size, self.output_size)  # [timesteps, batch_size, output_size]

        return output, hidden_state