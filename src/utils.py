import os
import torch


def create_output_dir(output_dir, project_name):

    output_project_dir = os.path.join(output_dir, project_name)

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    if not os.path.exists(output_project_dir):
        os.mkdir(output_project_dir)


def config_devices(gpu_id=0, seed=42, cpu_performance='standard', force_cpu=False):

    ###########################################################################################

    def set_cpu(cpu_performance='standard', num_threads=torch.get_num_threads()):

        # If cpu_performance = 'custom', you must define num_threads.
        # By default it uses half of the cores (cpu_performance = 'standard')

        print('.' * 10)
        print(' \n [ BEFORE ] NUM THREADS = {} \n '.format(torch.get_num_threads()))

        if cpu_performance == 'min':

            num_threads = 1
            torch.set_num_threads(num_threads)

        elif cpu_performance == 'max':

            num_threads = 2 * torch.get_num_threads()
            torch.set_num_threads(num_threads)

        elif cpu_performance == 'custom':

            torch.set_num_threads(num_threads)

        else:  # cpu_performance == 'standard':

            pass

        print(' \n [ NOW ] NUM THREADS = {} \n '.format(torch.get_num_threads()))
        print('.' * 10)


    ###########################################################################################

    def set_seeds(seed):

        if torch.cuda.is_available():

            torch.cuda.manual_seed(seed)
            torch.cuda.manual_seed_all(seed)

            torch.manual_seed(seed)
            # torch.backends.cudnn.deterministic = True

        else:

            torch.manual_seed(seed)


    ###########################################################################################

    set_cpu(cpu_performance=cpu_performance)
    set_seeds(seed=seed)

    device = (torch.device('cpu') if force_cpu else torch.device('cuda:' + str(gpu_id) if torch.cuda.is_available() else 'cpu'))

    print('\n device : {} \n'.format(device))

    return device
