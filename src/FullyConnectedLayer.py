import torch


class FullyConnectedLayer(torch.nn.Module):

    def __init__(self,
                 device,
                 input_size,
                 output_size,
                 bias=True,
                 activation_function='linear'
                 ):

        super(FullyConnectedLayer, self).__init__()

        self.device = device

        self.input_size = input_size
        self.output_size = output_size
        self.bias = bias
        self.activation_function = activation_function

        # linear layer
        self.linear = torch.nn.Linear(in_features=self.input_size,
                                      out_features=self.output_size,
                                      bias=self.bias
                                      ).to(self.device)

        # activation Function
        if self.activation_function == 'tanh':
            self.activation = torch.nn.Tanh()

        elif self.activation_function == 'relu':
            self.activation = torch.nn.ReLU(inplace=False)

        elif self.activation_function == 'sigmoid':
            self.activation = torch.nn.Sigmoid()

        elif self.activation_function == 'linear':
            pass

        else:
            raise NotImplementedError

        # initialize parameters
        self.init_weights()

    def init_weights(self):
        torch.nn.init.normal_(tensor=self.linear.weight.data,
                              mean=0.0,
                              std=1e-1
                              )
        if self.bias:
            torch.nn.init.constant_(tensor=self.linear.bias.data,
                                    val=0.0
                                    )

    def forward(self, input):

        # linear output
        output = self.linear(input)

        # activation
        if self.activation_function != 'linear':
            output = self.activation(output)

        return output
