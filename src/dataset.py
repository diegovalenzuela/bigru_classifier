import os
import numpy as np
import random
from glob import glob
from random import shuffle

import torch
from torch.utils.data import Dataset
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence as pack

# vocabulary_size = len(residues) + 1  # +1 porque hay padding

########################################################################################################################

residues = ['PRO', 'TYR', 'THR', 'VAL', 'PHE', 'ARG', 'GLY', 'CYS', 'ALA',
            'LEU', 'MET', 'ASP', 'GLN', 'SER', 'TRP', 'LYS', 'GLU', 'ASN',
            'ILE', 'HIS']  # , 'UNK']

seq_structure = ["H", "S", "B"]


def get_residues_names():
    return residues


def res2emb(residue):

    residue = residue.upper()

    if residue in residues:
        emb = residues.index(residue) + 1
        return emb

    else:
        return None


def sec2emb(ss):

    ss = ss.upper()

    if ss not in seq_structure:
        ss = secondary_structure_compress(ss)

    return seq_structure.index(ss) + 1


def secondary_structure_compress(ss):

    if ss == 'G' or ss == 'I':
        return 'H'

    elif ss == 'T' or ss == 'S':
        return 'S'

    else:
        return 'B'


def emb2res(index):
    return residues[index - 1]


########################################################################################################################


def set_dataset(path_to_data, normalize, **kwargs):

    train_batch_size = kwargs['train_batch_size']
    val_batch_size = kwargs['val_batch_size']
    test_batch_size = kwargs['test_batch_size']
    use_cuda = kwargs['use_cuda']

    if kwargs['debug_examples'] is not None:
        debug_examples = kwargs['debug_examples']
    else:
        debug_examples = 10

    if kwargs['debug'] is not None:
        debug = kwargs['debug']
        if debug:
            print('\n DEBUG MODE IS \t : \t ON ! \n')
    else:
        debug = False

    if kwargs['randomize'] is not None:
        randomize = kwargs['randomize']
    else:
        randomize = False

    print(' randomize : {} '.format(randomize))

    if kwargs['reverse'] is not None:
        reverse = kwargs['reverse']
    else:
        reverse = True

    print(' reverse : {} '.format(reverse))

    train_dir = os.path.join(path_to_data, 'train')
    dev_dir = os.path.join(path_to_data, 'dev')
    test_dir = os.path.join(path_to_data, 'test')

    # Normalization

    path_all_data = os.path.join(path_to_data, '*')

    max_vector = 1.0

    if normalize:

        all_data = ProteinsDataset(path_all_data,
                                   sep=',',
                                   debug=debug,
                                   debug_examples=debug_examples,
                                   reverse=reverse
                                   )

        # GET MAX VECTOR #

        for idx in range(0, len(all_data)):
            _, _, _, _, _ = all_data[idx]
            max_vector = all_data.max_vector

    # Protein Data Set

    data_train = ProteinsDataset(train_dir,
                                 sep=',',
                                 normalize=normalize,
                                 max_vector=max_vector,
                                 debug=debug,
                                 debug_examples=debug_examples,
                                 reverse=reverse
                                 )

    data_eval = ProteinsDataset(dev_dir,
                                sep=',',
                                normalize=normalize,
                                max_vector=max_vector,
                                debug=debug,
                                debug_examples=debug_examples,
                                reverse=reverse
                                )

    data_test = ProteinsDataset(test_dir,
                                sep=",",
                                normalize=normalize,
                                max_vector=max_vector,
                                debug=debug,
                                debug_examples=debug_examples,
                                reverse=reverse
                                )

    # Protein Wrapper

    data_train = WrapperProtein(data_set=data_train,
                                batch_size=train_batch_size,
                                use_cuda=use_cuda,
                                randomize=randomize,
                                reverse=reverse
                                )

    data_eval = WrapperProtein(data_set=data_eval,
                               batch_size=val_batch_size,
                               use_cuda=use_cuda,
                               randomize=randomize,
                               reverse=reverse
                               )

    data_test = WrapperProtein(data_set=data_test,
                               batch_size=test_batch_size,
                               use_cuda=use_cuda,
                               randomize=randomize,
                               reverse=reverse
                               )

    return data_train, data_eval, data_test, max_vector


########################################################################################################################


class ProteinsDataset(Dataset):

    def __init__(self, root_dir, sep=",", normalize=False, max_vector=None, debug=False, **kwargs):

        super(Dataset, self).__init__()

        if kwargs['debug_examples'] is not None:
            self.debug_examples = kwargs['debug_examples']
        else:
            self.debug_examples = 10

        if kwargs['reverse'] is not None:
            self.reverse = kwargs['reverse']
        else:
            self.reverse = True

        self.root_dir = root_dir
        self.sep = sep
        self.N_CA_C_angle = 111.068
        self.CA_C_N_angle = 116.64
        self.C_N_CA_angle = 121.38

        self.names = sorted(glob(os.path.join(self.root_dir, '*.csv')), reverse=self.reverse)

        if normalize and max_vector is None:
            raise ValueError("To normalize need a max_vector parameter")

        self.normalize = normalize
        self.max_vector = max_vector

        self.debug = debug

    def __len__(self):
        return len(self.names) if not self.debug else self.debug_examples

    def __getitem__(self, idx):

        res_num = -1
        atom_list = []
        pos_list = []
        flag_atom = False

        sequence_residues = []
        sequence_secondary_structure = []
        angles = []
        sequence_atoms = []
        positions = []

        chain_res = None

        with open(self.names[idx], "r") as pdb:

            for index, line in enumerate(pdb):

                if index == 0:
                    continue  # HEADER

                # CSV STRUCTURE
                # CHAIN, RESNAME, RES_N, ATOM_TYPE, X, Y, Z, SS, PHI, PSI, OMEGA

                residue_values = line.split(self.sep)

                chain = residue_values[0].upper()
                resname = residue_values[1].upper()
                res_num_i = residue_values[2]
                atom = residue_values[3].upper()
                pos_x = residue_values[4]
                pos_y = residue_values[5]
                pos_z = residue_values[6]
                sec_str = residue_values[7].upper()
                ang_phi = residue_values[8].strip()
                ang_psi = residue_values[9].strip()
                ang_ome = residue_values[10].strip()

                res_num_i = int(res_num_i)

                if chain_res is None:
                    chain_res = chain

                if chain != chain_res:
                    break

                if pos_x == "" or pos_y == "" or pos_z == "" or ang_phi == "" or ang_psi == "" or ang_ome == "\n":
                    continue

                if ang_phi == "None":
                    ang_phi = 0
                if ang_psi == "None":
                    ang_psi = 0
                if ang_ome == "None":
                    ang_ome = 180

                if res_num_i != res_num and flag_atom == False:
                    assert len(sequence_residues) == len(sequence_secondary_structure) == len(angles)
                    assert len(sequence_atoms) == len(positions) == len(angles)
                    res_num = res_num_i
                    res_emb = res2emb(resname)

                    if res_emb is not None:
                        sequence_residues.append(res_emb)  # append numpy shape (1, num_residues)
                    else:
                        continue

                    sequence_secondary_structure.append(sec2emb(sec_str))  # append numpy shape (1, 3)

                    angles.append(np.array([float(ang_phi) * np.pi / 180,
                                            float(ang_psi) * np.pi / 180,
                                            float(ang_ome) * np.pi / 180
                                            ]
                                           ).reshape(1, -1)
                                  )

                    atom_list = []
                    pos_list = []

                    flag_atom = True

                if (atom == "N" or atom == "CA" or atom == "C") and (res_num_i == res_num) and flag_atom == True:
                    atom_list.append(atom)
                    pos_list.append([float(pos_x),
                                     float(pos_y),
                                     float(pos_z)]
                                    )
                    if len(atom_list) == 3 and len(pos_list) == 3:
                        sequence_atoms.append(atom_list)
                        positions.append(np.array(pos_list).reshape(-1, 3))  # append numpy shape (3, 3)
                        flag_atom = False

        sequence_residues = np.stack(sequence_residues, axis=0)
        sequence_residues = sequence_residues.reshape(sequence_residues.shape[0], -1)

        sequence_secondary_structure = np.array(sequence_secondary_structure)
        sequence_secondary_structure = sequence_secondary_structure.reshape(sequence_secondary_structure.shape[0], -1)

        angles = np.stack(angles, axis=0)
        angles = angles.reshape(angles.shape[0], -1)

        positions = np.stack(positions, axis=0)
        positions = positions.reshape(-1, 3)

        center = positions.sum(axis=0) / positions.shape[0]
        positions = positions - center

        norm = np.sqrt(
            positions[:, 0] * positions[:, 0] + positions[:, 1] * positions[:, 1] + positions[:, 2] * positions[:, 2])
        max_vector = np.max(norm)

        if self.max_vector is None or max_vector > self.max_vector:
            self.max_vector = max_vector
            # print("\nmax vector in file = {}\n".format(self.names[idx])) # TODO : uncomment this

        if self.normalize:
            positions = positions / self.max_vector

        # print('NAMES : {} '.format( self.names[idx] ) )

        positions = positions.reshape(sequence_residues.shape[0], -1)  # Pongo esto para poder predecir posiciones

        return sequence_residues, angles, positions, sequence_secondary_structure, self.names[idx]


class WrapperProtein:

    def __init__(self, data_set, batch_size, use_cuda, randomize, reverse):

        self.data_set = data_set
        self.batch_size = batch_size
        self.index = [i for i in range(len(data_set))]

        self.randomize = randomize
        self.reverse = reverse

        if self.randomize:
            shuffle(self.index)  # TODO: add if condition

        self.use_cuda = use_cuda
        self.actual_position = 0

    def next_batch(self):

        if self.actual_position + self.batch_size <= len(self.data_set):
            sub_index = self.index[self.actual_position: self.actual_position + self.batch_size]
            self.actual_position += self.batch_size
        else:
            sub_index = self.index[self.actual_position:]
            self.actual_position = self.batch_size - len(sub_index)
            sub_index += self.index[: self.batch_size - len(sub_index)]

        aa = []
        angles = []
        coords = []
        lengths_aa = []
        lengths_coords = []
        ss = []

        names = []

        batch = [self.data_set[i] for i in sub_index]

        for (a, b, c, d, e) in sorted(batch, key=self.getKey, reverse=self.reverse):  # TODO:  for (a, b, c, d, e) in batch:

            aa.append(torch.LongTensor(a))
            angles.append(torch.FloatTensor(b))
            coords.append(torch.FloatTensor(c))
            lengths_aa.append(len(a))
            lengths_coords.append(len(c))
            ss.append(torch.LongTensor(d))
            names.append(e)

        packed_aa = self.stack_variable(aa, dtype=torch.long)
        packed_ss = self.stack_variable(ss, dtype=torch.long)
        packed_angles = self.stack_variable(angles)
        packed_coords = self.stack_variable(coords)

        return packed_aa, packed_angles, packed_coords, lengths_aa, lengths_coords, packed_ss, names

    @staticmethod
    def getKey(item):
        return len(item[0])

    def __len__(self):
        return len(self.data_set) // self.batch_size + 1

    def stack_variable(self, var, dtype=torch.float):
        max_len, n_feats = var[0].size()  # Sabe que en 0 est� la mas larga
        var = [torch.cat((s, torch.zeros(max_len - s.size(0), n_feats, dtype=dtype)), 0) if s.size(0) != max_len else s
               for s in var]
        var = torch.stack(var, 0)
        if self.use_cuda:
            var = Variable(var).cuda()
        else:
            var = Variable(var)

        return var
