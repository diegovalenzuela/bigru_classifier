import torch


class EmbeddingLayer(torch.nn.Module):

    def __init__(self,
                 device,
                 num_embeddings=21,
                 embedding_dim=8,
                 padding_idx=0,
                 max_norm=None,
                 scale_grad_by_freq=False,
                 sparse=False
                 ):

        super(EmbeddingLayer, self).__init__()

        self.device = device

        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim

        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.scale_grad_by_freq = scale_grad_by_freq
        self.sparse = sparse

        # embedding layer
        self.embedding = torch.nn.Embedding(num_embeddings=self.num_embeddings,
                                            embedding_dim=self.embedding_dim,
                                            padding_idx=self.padding_idx,
                                            max_norm=self.max_norm,
                                            norm_type=2,
                                            scale_grad_by_freq=self.scale_grad_by_freq,
                                            sparse=self.sparse,
                                            _weight=None
                                            ).to(self.device)

        # initialize parameters
        self.init_weights()

    def init_weights(self):

        torch.nn.init.uniform_(tensor=self.embedding.weight.data,
                               a=-1e-1,
                               b=1e-1
                               )

    def forward(self, input):

        output = self.embedding(input)

        return output