import os
import time

import torch
from tensorboardX import SummaryWriter

from src.utils import config_devices
from src.dataset import set_dataset, get_residues_names

from src.BiRNN import BiRNNModel

from src.utils import create_output_dir
from src.helpers import run_train_epoch, run_evaluation

import numpy as np

seed = 42
np.random.seed(seed=seed+1)


########################################################################################################################

residues = get_residues_names()
vocabulary_size = len(residues) + 1  # +1 porque hay 1 símbolo para hacer padding

# Config Devices

gpu_id = 1  # 0 to use cuda:0 device, 1 to use cuda:1 device, etc
force_cpu = False
cpu_performance = 'max'  # 'min', 'custom', 'standard'
seed = 42
use_cuda = torch.cuda.is_available()


# Config Data Set

debug = True  #  TODO : CHECK THIS !
debug_examples = 10

path_to_data = '/bacondata/DIEGO/data50mil/'
normalize = False

randomize = False
reverse = True  # if True los ordena de mayor a menor longuitud, if False los ordena de menor a mayor longuitud


# ñe

num_experiment = 1

suffix = '_all'

if debug:
    suffix = '_debug'


output_dir = '/bacondata/DIEGO/'
project_name = 'BiGRU_Classifier' + '_' + str(num_experiment) + suffix


save_every = 1

if debug:
    save_every = 10000


log_interval = 1000
val_log_interval = 1
scale_by = 1.0  # escalar loss en el print


epochs = 1000  # TODO: CHANGE THIS !


# hyper parameters

input_size = 3  # dihedral angles
output_size = vocabulary_size

initial_learning_rate = 1e-3


use_embeddings = False
embedding_dim = 20  # not in use


hidden_size = 16
num_layers = 2
dropout = 0.0

clip_by_norm = True
max_norm = 1.0  # clip_by_norm

recurrent = 'gru'  # 'lstm'
bidirectional = True
batch_first = False


# BATCH SIZE

train_batch_size = 1
val_batch_size = 1
test_batch_size = 1


# Retrain


retraining = False
last_epoch = 0

if not retraining:
    last_epoch = 0
else:
    epochs = epochs - last_epoch


########################################################################################################################


device = config_devices(gpu_id=gpu_id,
                        seed=seed,
                        cpu_performance=cpu_performance,
                        force_cpu=force_cpu
                        )


print(' start set_dataset ')

ti = time.time()

data_train, data_eval, data_test, max_vector = set_dataset(path_to_data=path_to_data,
                                                           normalize=normalize,
                                                           train_batch_size=train_batch_size,
                                                           val_batch_size=val_batch_size,
                                                           test_batch_size=test_batch_size,
                                                           use_cuda=use_cuda,
                                                           debug=debug,
                                                           debug_examples=debug_examples,
                                                           randomize=randomize,
                                                           reverse=reverse
                                                           )

tf = time.time()
dt = (tf - ti) / 60.0

print('\n end set_dataset \t\t\t  dt : {:.2f} \n'.format(dt))

print('\n max_vector : \n {} \n'.format(max_vector))


model = BiRNNModel(device=device,
                   input_size=input_size,
                   output_size=output_size,
                   embedding_dim=embedding_dim,
                   hidden_size=hidden_size,
                   num_layers=num_layers,
                   batch_first=batch_first,
                   dropout=dropout,
                   bidirectional=bidirectional,
                   recurrent=recurrent,
                   use_embeddings=use_embeddings
                   )

print('\n model : \n {} \n'.format(model))


# optimizer
optimizer = torch.optim.Adam(params=model.parameters(),
                             lr=initial_learning_rate,
                             betas=(0.9, 0.999),
                             eps=1e-08,
                             weight_decay=0.0,
                             amsgrad=True
                             )

print('\n optimizer : \n {} \n'.format(optimizer))


create_output_dir(output_dir=output_dir,
                  project_name=project_name
                  )


if retraining:
    # model
    model = BiRNNModel(device=device,
                       input_size=input_size,
                       output_size=output_size,
                       embedding_dim=embedding_dim,
                       hidden_size=hidden_size,
                       num_layers=num_layers,
                       batch_first=batch_first,
                       dropout=dropout,
                       bidirectional=bidirectional,
                       recurrent=recurrent,
                       use_embeddings=use_embeddings
                       )

    # model path
    model_path = os.path.join(output_dir,
                              project_name,
                              project_name + '_model_epoch_{}.pt'.format(last_epoch)
                              )

    # load model
    model.load_state_dict(torch.load(model_path,
                                     map_location=device
                                     )
                          )

    # model to device
    model.to(device)

    # optimizer
    optimizer = torch.optim.Adam(params=model.parameters(),
                                 lr=initial_learning_rate,
                                 betas=(0.9, 0.999),
                                 eps=1e-08,
                                 weight_decay=0.0,
                                 amsgrad=True
                                 )

    # optimizer path
    optimizer_path = os.path.join(output_dir,
                                  project_name,
                                  project_name + '_model_optimizer_epoch_{}.pt'.format(last_epoch)
                                  )

    # load optimizer
    optimizer.load_state_dict(torch.load(optimizer_path))


########################################################################################################################


epoch_num_list = []

train_loss_per_epoch = []
val_loss_per_epoch = []

start_time = time.time()


writer = SummaryWriter('./results/' + project_name)


for epoch in range(epochs):

    ti = time.time()

    model, optimizer, loss, train_loss_history = run_train_epoch(model=model,
                                                                 optimizer=optimizer,
                                                                 data=data_train,
                                                                 batch_size=train_batch_size,
                                                                 device=device
                                                                 )

    model.zero_grad()

    tf = time.time()

    dt = (tf - ti) / 60.0

    # training log
    if False:  # if (epoch + 1) % log_interval == 0 and (epoch + 1) % val_log_interval != 0:

        current_time = time.time()
        elapsed_time = (current_time - start_time) / 60.0

        print('epoch : [{:10d}/{:10d}] , '
              'loss : {:f} +/- {:f} '
              'dt : {:.2f} , '
              'elapsed_time : {:.2f} '
              ''.format(epoch + last_epoch + 1,
                        epochs + last_epoch,
                        np.mean(train_loss_history) * scale_by,
                        np.std(train_loss_history) * scale_by,
                        dt,
                        elapsed_time
                        )
              )

    # validation log
    if (epoch + 1) % val_log_interval == 0 or epoch == 0:
        val_loss, val_loss_history = run_evaluation(device=device,
                                                    model=model,
                                                    data=data_eval,
                                                    batch_size=val_batch_size
                                                    )

        writer.add_scalars('loss',
                           {'training_loss': loss,
                            'validation_loss': val_loss
                            },
                           epoch + last_epoch
                           )

        current_time = time.time()
        elapsed_time = (current_time - start_time) / 60.0

        print('epoch : [{:10d}/{:10d}] , '
              'loss : {:f} +/- {:f} , '
              'val loss : {:f} +/- {:f} , '
              'dt : {:.2f} , '
              'elapsed_time : {:.2f} '
              ''.format(epoch + last_epoch + 1,
                        epochs + last_epoch,
                        np.mean(train_loss_history) * scale_by,
                        np.std(train_loss_history) * scale_by,
                        np.mean(val_loss_history) * scale_by,
                        np.std(val_loss_history) * scale_by,
                        dt,
                        elapsed_time
                        )
              )

        epoch_num_list.append(epoch)

        train_loss_per_epoch.append(np.mean(train_loss_history))
        val_loss_per_epoch.append(np.mean(val_loss_history))

    # saving model every 'save_every' epochs

    if (epoch + 1) % save_every == 0 or epoch == 0:
        # save model
        torch.save(obj=model.state_dict(),
                   f=os.path.join(output_dir,
                                  project_name,
                                  project_name + '_model_epoch_{}.pt'.format(epoch)
                                  )
                   )

        # save optimizer
        torch.save(obj=optimizer.state_dict(),
                   f=os.path.join(output_dir,
                                  project_name,
                                  project_name + '_model_optimizer_epoch_{}.pt'.format(epoch)
                                  )
                   )

        print(' \n model saved at epoch : \t {} \n '.format(epoch + last_epoch + 1))
